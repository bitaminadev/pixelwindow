	<footer>
		<div class="footerRoot">
			<div class="footerNode">
				<div class="footerLeaf">
					<p class="footerTitle">Nosotros</p>
					<hr class="footerHr">
					<p><a href="">Pixel Window</a></p>
					<p><a href="">Casos de éxito</a></p>
					<p><a href="">Más información</a></p>
					<br>
					<hr class="footerHr">
					<p><a href="">Catálogo en línea</a></p>
				</div>
				<div class="footerLeaf">
					<p class="footerTitle">Servicios</p>
					<hr class="footerHr">
					<p><a href="">Soluciones interactivas</a></p>
					<p><a href="">Kioscos digitales interactivos</a></p>
					<p><a href="">Señalización digital</a></p>
					<p><a href="">Videowall</a></p>
					<p><a href="">Pintura de proyección</a></p>
					<p><a href="">Servicios de operación</a></p>
					<p><a href="">Cristales inteligentes</a></p>
					<p><a href="">Pantallas LED</a></p>
				</div>
			</div>
			<div>
				<div class="footerNodeTitle">
					<p class="footerTitle">Contacto</p>
					<hr class="footerHr">
				</div>
				<div class="footerNode all">
					<div class="footerLeaf">
						<p class="footerTitle">MATRIZ QUERÉTARO, QRO</p>
						<span>Blvd. Bernardo Quintana No. 23</span>
						<span>Entrada por Aile #2  Piso 1</span>
						<span>Col. Álamos 2da Sección</span>
						<span>C.P. 76160 Querétaro, Qro.</span>
						<br>
						<span>Tel. 01 (442) 456 4968</span>
					</div>
					<div class="footerLeaf">
						<p class="footerTitle">SUCURSAL CD.MX</p>
						<span>Circuito Circunvalación Pte. 9,</span>
						<span>Cd. Satélite.</span>
						<span>Naucalpan de Juárez, Méx.</span>
						<span>C.P. 53100</span>
						<br>
						<span>Tel. 01 (55) 4435 9615</span>
					</div>
				</div>
			</div>
		</div>
		<div class="footerRoot">
			<div class="footerNode all second">
				<div class="footerLeaf">
					<p class="footerTitle">MATRIZ QUERÉTARO, QRO</p>
					<span>Blvd. Bernardo Quintana No. 23</span>
					<span>Entrada por Aile #2  Piso 1</span>
					<span>Col. Álamos 2da Sección</span>
					<span>C.P. 76160 Querétaro, Qro.</span>
					<br>
					<span>Tel. (33) 1454 2201</span>
				</div>
				<div class="footerLeaf">
					<p class="footerTitle">SUCURSAL CD.MX</p>
					<span>Circuito Circunvalación Pte. 9,</span>
					<span>Cd. Satélite.</span>
					<span>Naucalpan de Juárez, Méx.</span>
					<span>C.P. 53100</span>
					<br>
					<span>Tel. +1 442 666 1190</span>
				</div>
			</div>
			<div class="footerNode all first">
				<div class="footerLeaf all">
					<p class="footerTitle">Aceptamos tarjetas de crédito o débito</p>
					<p>
						<img src="<?php echo $httpProtocol.$host.$url.'img/icons/icono-tarjetas.webp'?>" alt="tarjetas de crédito y débito">
					</p>
					<p>
						<img src="<?php echo $httpProtocol.$host.$url.'img/icons/icono-youtube.webp'?>" alt="youtube">
						<img src="<?php echo $httpProtocol.$host.$url.'img/icons/icono-facebook.webp'?>" alt="facebook">
					</p>
				</div>
			</div>
		</div>
		<div class="footerEmail">
			info@pixelwindow.com.mx
		</div>
		<div class="copyright">
			<p>
				&copy; Copyright 2011-2019 Pixel Window SA de CV
			</p>
			<p>
				<a href=''>Todos los derechos reservados</a> | <a href=''>Aviso de privacidad</a>
			</p>
		</div>
	</footer>
	<?php echo $js;?>
	</body>
</html>