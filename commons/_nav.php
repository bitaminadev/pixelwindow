<nav>
	<div class="logoContainer">
		<img src="<?php echo $httpProtocol.$host.$url.'img/logopixelwindow.png'?>" alt='pixelwindow logo' class='logoNav'>
	</div>
	<div class="navContent">
		<div class="contactNavContainer">
			<ul class="contactNav">
				<li class="contactNavOption">
					<span class="boldText">CD.MX</span>
					01 (55) 12 04 14 51
				</li>
				<li class="contactNavOption">
					<span class="boldText">QUERÉTARO</span>
					01 (442) 456 4968
				</li>
				<li class="contactNavOption">
					<span class="boldText">GUADALAJARA</span>
					01 (33) 1454 2201
				</li>
				<li class="contactNavOption">
					<span class="boldText">US</span>
					+1 442 666 1190
				</li>
			</ul>
		</div>
		<ul class="menuNav">
			<li class="menuNavOption">
				Nosotros
			</li>
			<li class="menuNavOption">
				Señalización digital
			</li>
			<li class="menuNavOption">
				Video Wall
			</li>
			<li class="menuNavOption">
				Productos
			</li>
			<li class="menuNavOption">
				Casos de Éxito
			</li>
			<li class="menuNavOption">
				Contacto
			</li>
		</ul>
	</div>
</nav>