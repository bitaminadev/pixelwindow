<section id="introduction">
	<span class="slogan">Atrae las miradas de tus clientes</span>
	<h1>¡con Digital Signage - Señalización Digital!</h1>
	<p>
		Venta, instalación, capacitación, asesoría y desarrollo de contenido audiovisual para tus Video Walls.<br>
		Pantallas, Kioskos, etc. Genera mayor impacto, aumenta tu mercado, reduce costos, incrementa tus ventas<br>
		<span class='boldText'>y crea una experiencia digital interactiva con la más alta tecnología en Digital Signage.</span>
	</p>
</section>