<div id="offer">
		<div class="cardTransitive">
			<div class="cardTransitiveImage">
				<img src="<?php echo $httpProtocol.$host.$url.'img/fondo-hombres-negocios.webp'?>" alt="soluciones interactivas" class='beforeTransition'>
				<img src="<?php echo $httpProtocol.$host.$url.'img/img-banner02.webp'?>" alt="soluciones interactivas" class='afterTransition'>
			</div>
			<div class="cardTransitiveContent">
				<div class="beforeTransition">
					<h3 class="cardTransitiveTitle">soluciones interactivas</h3>
					<img src="<?php echo $httpProtocol.$host.$url.'img/icons/icono-interactivo.webp'?>" alt="producto" class="cardIcon">
					<ul>
						<li>Digital Signage (Señalización Digital)</li>
						<li>Experiencias interactivas</li>
						<li>Directorios interactivos</li>
						<li>Video Wall</li>
						<li>Cristal inteligente</li>
						<li>Pintura de proyección</li>
						<li>Pantallas LED</li>
						<li>Publicidad Digital</li>
						<li>Comunicación corporativa</li>
					</ul>
				</div>
				<div class="afterTransition">
					<h3 class="cardTransitiveTitle">soluciones interactivas</h3>
					<hr class="redBorder">
					<ul>
						<li>Digital Signage (Señalización Digital)</li>
						<li>Experiencias interactivas</li>
						<li>Directorios interactivos</li>
						<li>Video Wall</li>
						<li>Cristal inteligente</li>
						<li>Pintura de proyección</li>
						<li>Pantallas LED</li>
						<li>Publicidad Digital</li>
						<li>Comunicación corporativa</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="cardTransitive">
			<div class="cardTransitiveImage">
				<img src="<?php echo $httpProtocol.$host.$url.'img/img-pantallas.webp'?>" alt="soluciones interactivas" class='beforeTransition'>
				<img src="<?php echo $httpProtocol.$host.$url.'img/img-banner02.webp'?>" alt="soluciones interactivas" class='afterTransition'>
			</div>
			<div class="cardTransitiveContent">
				<div class="beforeTransition">
					<h3 class="cardTransitiveTitle">venta de productos</h3>
					<img src="<?php echo $httpProtocol.$host.$url.'img/icons/icono-producto.png'?>" alt="producto" class="cardIcon">
					<ul>
						<li>Video Wall</li>
						<li>Digital Signage</li>
						<li>Pantallas</li>
						<li>Equipo de audio</li>
						<li>Computadoras</li>
						<li>Impresoras</li>
						<li>Kioscos</li>
						<li>Y más...</li>
					</ul>
				</div>
				<div class="afterTransition">
					<h3 class="cardTransitiveTitle">venta de productos</h3>
					<hr class="redBorder">
					<ul>
						<li>Video Wall</li>
						<li>Digital Signage</li>
						<li>Pantallas</li>
						<li>Equipo de audio</li>
						<li>Computadoras</li>
						<li>Impresoras</li>
						<li>Kioscos</li>
						<li>Y más...</li>
					</ul>
				</div>
			</div>
		</div>
	</div>