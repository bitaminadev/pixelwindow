<!DOCTYPE html>
<?php
	// Direccion de la pagina
	$httpProtocol = "https://";
    $host = $_SERVER['SERVER_NAME'];
    $url = "/pixelwindow/";

    // Variables que almacenan el css y el js de la pagina
    $js = '<script src="'.$httpProtocol.$host.$url.'js/init.js"></script>';
    $css = '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/style.css">';
?>
<html lang="es">
<head>
	<title><?php echo $title?></title>
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $httpProtocol.$host.$url.'img/favicon/apple-touch-icon.png'?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $httpProtocol.$host.$url.'img/favicon/favicon-32x32.png'?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $httpProtocol.$host.$url.'img/favicon/favicon-16x16.png'?>">
    <link rel="manifest" href="<?php echo $httpProtocol.$host.$url.'img/favicon/site.webmanifest'?>">
    <link rel="mask-icon" href="<?php echo $httpProtocol.$host.$url.'img/favicon/safari-pinned-tab.svg'?>" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#00aba9">
    <meta name="theme-color" content="#ffffff">
    <meta charset="utf-8">