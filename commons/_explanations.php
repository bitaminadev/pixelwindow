<div id="explanations">
		<div class="cardInfo">
			<div class="cardInfoImage">
				<img src="<?php echo $httpProtocol.$host.$url.'img/img-banner01.webp'?>" alt="que es digital signage?">
				<div class="transparentMask"></div>
				<h3 class="cardInfoTitle">
					¿Qué es Digital Signage?
				</h3>
			</div>
			<div class="cardInfoContent">
				<span>Expertos en</span>
				<h3 class="cardInfoTitle">
					¿Qué es Digital Signage?
				</h3>
				
				<p>
					Transmisión de contenido audiovisual, para comunicación externa con fines informativos o publicitarios y comunicación interna para mostrar KPI u otros.
				</p>
				<p>
					Con nuestros softwares diseña, reproduce, controla y programa desde un solo lugar.
				</p>
			</div>
		</div>
		<div class="cardInfo">
			<div class="cardInfoImage">
				<img src="<?php echo $httpProtocol.$host.$url.'img/img-banner02.webp'?>" alt="que es un video wall?">
				<div class="transparentMask"></div>
				<h3 class="cardInfoTitle">
					¿Qué es un Video Wall?
				</h3>
			</div>
			<div class="cardInfoContent">
				<span>Especialistas en</span>
				<h3 class="cardInfoTitle">
					¿Qué es un Video Wall?
				</h3>
				
				<p>
					Configuración de monitores profesionales que muestran una sola imagen en gran formato y la mejor calidad, ontrola tu pantallas
				</p>
			</div>
		</div>
	</div>