<?php 
	$title = 'PixelWindow';
	$description = '';
	$keywords = '';

	include('commons/_headOpen.php');

	$css.='<div id="fb-root"></div><link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/index.css">';
	$js.='<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.3"></script>';
	// If hreflang is set, it will be included in the head.
	//$hreflang = '';
	// If canonical is set, it will be included in the head
	//$canonical = '';

	include('commons/_headClose.php');
	
	echo '<header>';
	include ('commons/_nav.php');
	echo '<video class="headerVideo" autoplay muted loop>
			<source src="video/pixelwindowvideo.webmhd.webm" type="video/webm">
			<source src="video/pixelwindowvideo.oggtheora.ogv" type="video/ogv">
			<source src="video/pixelwindowvideo.mp4" type="video/mp4">
			<img src="'.$httpProtocol.$host.$url.'"img/indexHeaderBanner.jpg" title="Your browser does not support the <video> tag" alt="video not supported, see this image">
		</video>
	</header>';
	include ('commons/_introduction.php'); 
	include ('commons/_explanations.php');
	include ('commons/_offer.php');
	include ('commons/_allyes.php');
	include ('commons/_otherMarks.php');
?>
	<div class="informativeBanner">
		<h3 class="informativeBannerText">Líderes en Digital Signage, Video Wall y soluciones Interactivas para empresas</h3>
	</div>
	<div id="finalContent">
		<div id ="finalContentRow">
			<div class="computerContainer">
				<div class="computerContent">
					<video autoplay muted loop controls>
						<source src="video/pixelwindowvideo.webmhd.webm" type="video/webm">
						<source src="video/pixelwindowvideo.oggtheora.ogv" type="video/ogv">
						<source src="video/pixelwindowvideo.mp4" type="video/mp4">
						<img src="<?php echo $httpProtocol.$host.$url.'img/indexHeaderBanner.jpg'?>" title="Your browser does not support the <video> tag" alt="video not supported, see this image">
					</video>
				</div>
			</div>
			<div class="fb-page" data-href="https://www.facebook.com/pixelwindow/" data-tabs="timeline" data-width="320" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
				<blockquote cite="https://www.facebook.com/pixelwindow/" class="fb-xfbml-parse-ignore">
					<a href="https://www.facebook.com/pixelwindow/">PixelWindow facebook</a>
				</blockquote>
			</div>
		</div>
		<div>
			<img src="img/banner-certificados-senalizacion-digital.webp" alt="certificado" class="certificado">
			<button class="btn">Contáctanos</button>
		</div>
	</div>
<?php
	include('commons/_footer.php');
?>